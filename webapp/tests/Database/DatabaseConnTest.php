<?php

namespace App\Tests;

use App\Entity\Category;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class DatabaseConnTest extends KernelTestCase{
    /**
     * @var EntityManager
     */
    private $entityManager;

    protected function setUp(): void{
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }

    public function testSearchByCategoryName(){
        $category = $this->entityManager
            ->getRepository(Category::class)
            ->searchByCategoryName('Electronics')
        ;
        $this->assertCount(1, $category, 'Total count should be 1');
    }
    public function testSearchAllCategory(){
        $category = $this->entityManager
            ->getRepository(Category::class)
            ->getAll()
            //->findAll() should probably use this instead of getAll()
        ;
        $this->assertCount(20, $category, 'Total count should be 20');
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown(): void{
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
