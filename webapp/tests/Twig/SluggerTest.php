<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Twig\AppExtentionExtension;

class SluggerTest extends TestCase{
    /**
     * @dataProvider getSlugs
     * @param String $slug
     * @param String $string
     */
    public function testSlugify(String $string, String $slug){
        $slugger = new AppExtentionExtension();
        $this->assertSame($slug, $slugger->slugify($string));
    }

    public function getSlugs()
    {
        yield ['Lorem ipsum', 'lorem-ipsum'];
        yield [' Lorem ipsum ', 'lorem-ipsum'];
        yield [' lOrEm  iPsUm  ', 'lorem-ipsum'];
        yield ['!Lorem Ipsum!', 'lorem-ipsum'];
        yield ['lorem-ipsum', 'lorem-ipsum'];
        yield ['Children\'s books', 'childrens-books'];
        yield ['Five star movies', 'five-star-movies'];
        yield ['Adults 60+', 'adults-60'];

    }
}
